/**
 * Un ejemplo que modela un Circulo usando POO
 * 
 * @author (Milton JesÃºs Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class ModeloCirculo {

    float origenX;
    float origenY;
    float radio;
    float x;
    float y;

    public ModeloCirculo() {
    }

    public ModeloCirculo(float origenX, float origenY, float radio, float x, float y) {
        this.origenX = origenX;
        this.origenY = origenY;
        this.radio = radio;
        this.x = x;
        this.y = y;
    }

    public float getOrigenX() {
        return origenX;
    }

    public void setCentroX(float origenX) {
        this.origenX = origenX;
    }

    public float getOrigenY() {
        return origenY;
    }

    public void setCentroY(float origenY) {
        this.origenY = origenY;
    }

    public float getRadio() {
        return radio;
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }

    
    
    /**
     * 
     * @param x coordenada x del punto
     * @param y coordenada y del punto
     * @return
     */
    public String getPosicionPunto(float x, float y) {
        String ubicacion = "Ubicación Desconocida";

        if (this.puntoEstaDentro(x, y)) {
            ubicacion = "Punto Dentro del Círculo";
        } else if (this.puntoEstaEnBorde(x, y)) {
            ubicacion = "Punto En Borde de Círculo";
        } else {
            ubicacion = "Punto Fuera del Círculo";
        }

        return ubicacion;
    }//fin mÃ©todo getPosiciÃ³nPunto
    
    public boolean puntoEstaDentro(float x, float y) {
        double distancia =  Math.sqrt(Math.pow(this.getOrigenX()-x, 2) + Math.pow(this.getOrigenY()-y, 2));
        if(distancia<this.radio){
            return true;
        }
        return false;//complete
    }//fin mÃ©todo puntoEstaDentro 
    
    public boolean puntoEstaFuera(float x, float y) {
        double distancia =  Math.sqrt(Math.pow(this.getOrigenX()-x, 2) + Math.pow(this.getOrigenY()-y, 2));
        if(distancia>this.radio){
            return true;
        }
        return false;//complete
    }//fin mÃ©todo puntoEstaFuera
    
    public boolean puntoEstaEnBorde(float x, float y) {
        double distancia =  Math.sqrt(Math.pow(this.getOrigenX()-x, 2) + Math.pow(this.getOrigenY()-y, 2));
        if(distancia==this.radio){
            return true;
        }
        return false;//complete
    }//fin puntoEstaEnBordeSuperior
    
    public void setX(float x) {
        this.x = x;
    }
    
    public void setY(float y) {
        this.y = y;
    }
    
    public float getX() {
        return this.x;
    }
    
    public float getY() {
        return this.y;
    }
}//fin clase Circulo

