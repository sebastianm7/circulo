import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

public class ControladorCirculo {

    @FXML
    private TextField txtCentroX;

    @FXML
    private Label lblUbicacion;

    @FXML
    private Button cmdPosicion;

    @FXML
    private BorderPane panel1;

    @FXML
    private Button cmdCoordenadas;

    @FXML
    private TextField txtCentroY;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdAyuda;

    @FXML
    private Label lblY1;

    @FXML
    private Label lblX1;

    @FXML
    private Label lblCentroX;

    @FXML
    private Label lblCentroY;

    @FXML
    private TextField txtY1;

    @FXML
    private TextField txtX1;

    @FXML
    private ImageView imageCirculo;

    @FXML
    private GridPane panelBotones;

    @FXML
    private Label lblRadio;

    @FXML
    private TextField txtRadio;
    
    private ModeloCirculo circulo;
    
    public ControladorCirculo(){
    circulo = new ModeloCirculo();
    }
    
    @FXML
    void actualizarCoordenadas() {
    circulo.setCentroX(Float.parseFloat(txtCentroX.getText()));
    circulo.setCentroY(Float.parseFloat(txtCentroY.getText()));
    circulo.setRadio(Float.parseFloat(txtRadio.getText()));
    circulo.setX(Float.parseFloat(txtX1.getText()));
    circulo.setY(Float.parseFloat(txtY1.getText()));
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Medidas actualizadas");
    alert.showAndWait();
    }

    @FXML
    void determinarPosicion() {
    lblUbicacion.setText(circulo.getPosicionPunto(circulo.getX(), circulo.getY()));
    }

    @FXML
    void ayuda() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Determine la ubicación del punto con respecto al Círculo");
    alert.showAndWait();
    }
}
